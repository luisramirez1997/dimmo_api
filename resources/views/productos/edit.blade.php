
@extends('layouts.app')
@section('content')
<div class="container">

 
@include('partial.errors_validation')

	<form action=" {{ url('/productos/'.$producto->id) }} " method="post" enctype="multipart/form-data">
 {{csrf_field() }}
 {{ method_field('PATCH')}}


@include('productos.form_final',['Modo'=>'EDITANDO producto'])

			<input class="btn btn-success" type="submit" value="Editar">
			<a href="/productos" class="btn primary">Regresar</a>	

		</form>


</div>
@endsection