
@extends('layouts.app')
@section('content')
<div class="container">

<!-- //estamos condicionando que si da error nos muestre el error
 -->
@include('partial.errors_validation')
	<form method="post" action="{{ url('/productos') }}" class="form-horizontal" enctype="multipart/form-data">
 
{{ csrf_field() }}

			@include('productos.form_final',['Modo'=>'AGREGANDO EMPLEADO'])

			<input class="btn btn-success" type="submit" value="Agregar">
			<a href="/productos" class="btn primary">Regresar</a>	
	</form>


</div>
@endsection