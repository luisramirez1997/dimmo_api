
@extends('layouts.app')
@section('content')
<div class="container">


@include('partial.mensajes')




<nav class="navbar navbar-light bg-light">
<a href="{{ url('/productos/create') }}" class="btn btn-success"> Agregar Producto</a>


  <form method="post" action="{{ url('/productos/find') }}" class="form-inline">

  @csrf
    <input class="form-control mr-sm-2" required type="text" name="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>


</nav>


                 




<br/>
<br/>

<table class="table table-light table-hover">

<thead class="thead-light">
    <tr>

    <th> SKU </th>
    <th> Foto </th>
    <th> Nombre </th>
    <th> Descripcion </th>
    <th> Precio </th>
    <th> Acciones </th>
    
    </tr>
    </thead>
    <tbody>
        @foreach($productos as $producto)
        <tr>
            <td>{{$producto->sku}}</td> 

            <td>
            @if($producto->foto != null)
			<img src="/img/fotos/{{ $producto->foto }}" class="img-thumbnail img-fluid" width="100">
            @else
                <p>Sin Imagen</p>
            @endif
            </td>
            
            <td>{{$producto->nombre}} </td>
            <td>{{$producto->descripcion}} </td>
            <td>{{$producto->precio}}</td> 
          
            <td>
            <a class="btn btn-warning" href=" {{ url('/productos/'.$producto->id.'/edit') }}">
            Editar
            </a>           
            <form method="post" action="{{ url('/productos/'.$producto->id)  }}" style="display:inline">
            {{csrf_field() }}
            {{ method_field('DELETE') }}
            <button class="btn btn-danger" type="submit" onclick="return confirm('Borrar?');" >Borrar</button>
            </form>
            </td>       
        </tr>

        @endforeach

    </tbody>
</table>



{{ $productos->links()  }}

</div>
@endsection
