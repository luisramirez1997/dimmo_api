
@extends('layouts.app')
@section('content')
<div class="container">

 
@include('partial.errors_validation')

	<form action=" {{ url('/usuarios/'.$usuario->id) }} " method="post" enctype="multipart/form-data">
 {{csrf_field() }}
 {{ method_field('PATCH')}}


@include('usuarios.form_final',['Modo'=>'EDITANDO usuario'])

			<input class="btn btn-success" type="submit" value="Editar">
			<a href="/usuarios" class="btn primary">Regresar</a>	

		</form>


</div>
@endsection