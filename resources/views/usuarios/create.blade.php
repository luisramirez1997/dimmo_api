
@extends('layouts.app')
@section('content')
<div class="container">

<!-- //estamos condicionando que si da error nos muestre el error
 -->
@include('partial.errors_validation')
	<form method="post" action="{{ url('/usuarios') }}" class="form-horizontal" enctype="multipart/form-data">
 
{{ csrf_field() }}

			@include('usuarios.form_final',['Modo'=>'AGREGANDO EMPLEADO'])

			<input class="btn btn-success" type="submit" value="Agregar">
			<a href="/usuarios" class="btn primary">Regresar</a>	
	</form>


</div>
@endsection