@if(Session::has('Mensaje'))

<div class="container">
   <div class="alert alert-success alert-dismissible" id="myAlert">
    <a href="#" class="close">&times;</a>
    <strong>Success!</strong> {{    Session::get('Mensaje')   }} 
  </div>
</div>

<script>
$(document).ready(function(){
  $(".close").click(function(){
    $("#myAlert").alert("close");
  });
});
</script>

@endif
