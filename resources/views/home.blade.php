@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           

           <hr> 
           <passport-authorized-clients></passport-authorized-clients>
           
           <passport-personal-access-tokens></passport-personal-access-tokens>


        </div>
    </div>
</div>
@endsection
