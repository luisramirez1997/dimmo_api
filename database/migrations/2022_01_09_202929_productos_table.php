<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * SKU, nombre, cantidad, precio, descripción e imagen,
         *  siendo nombre, cantidad y precio campos obligatorios
         */
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku');
            $table->string('nombre')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('cantidad');
            $table->string('precio');
            $table->string('foto')->nullable();         
            $table->timestamps();
        });
    
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
        
    }
}
