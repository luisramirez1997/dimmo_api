<?php

namespace App\Http\Controllers;
// use Iluminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use Auth;
use App\User;


class MiLoginController extends Controller
{
    public function index()
    {
        $users = User::all();

        return User::all();

        // return response([

        // "user" => $users,


        //     ]);
            }


    public function login(Request $request){


        $credentials = $request->only('email', 'password');

        if( !Auth::attempt($credentials) ){
            return response([
                "message" => "User not valid sorry!!!"

            ],401);
        }


        $nombre_token = Auth()->user()->name;
        $nombre_token = $nombre_token . '_token';
        $accessToken = Auth::user()->createToken($nombre_token)->accessToken;

        return response([

        "user" => Auth::user(),
        "access_token" => $accessToken,

            ]);
    }
}
