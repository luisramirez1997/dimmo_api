<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['productos']=Producto::paginate(2);

        return view('productos.index',$datos);
    }



    public function find(Request $request){


        $datos['productos'] = Producto::Search($request->search)->orderby('created_at','DESC')->paginate(2);
        return view('productos.index',$datos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.create',[
            'producto' => New Producto
        ]);
    }

    public function store(Request $request){  

      //VALIDACION PARA NO DEJAR CAMPOS VACIOS
      $campos=[
            'nombre' => 'required|string|max:100',
            'cantidad' => 'required|integer',
            'precio' => 'required|numeric',
            'foto' => 'nullable|max:100000|mimes:jpeg,png,jpg'  

            // 'descripcion' => 'required|string|max:100',
            // 'sku' => 'required|string|max:100',
        ]; 

        $Mensaje=["required"=> 'El :attribute es requerido'];

        $this->validate($request,$campos,$Mensaje);
      //  $datosEmpleado=request()->all();

        $datosProducto=request()->except('_token');

        // if($request->hasFile('foto')){
        //     $datosProducto['foto']=$request->file('foto')->store('w','public');    
        // }
      

            //guardamos la imagen del libro
            if($request->hasFile('foto')){			
                $slug           = substr(strtoupper(sha1(time())),0,6);	
                    
                //guardamos la nueva foto
                $name           = $slug.'.'.$request->file('foto')->getClientOriginalExtension();
                $request->file('foto')->move(public_path().'/img/fotos', $name);
                $datosProducto['foto']   = $name;			           
            }	


       Producto::insert($datosProducto);
    //    return response()->json($datosEmpleado);   
    return redirect('productos')->with('Mensaje','Producto agregado con exito');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function show(cr $cr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        return view('productos.edit',compact('producto'));
    }

    public function update(Request $request, $id)
    {
                
//validacion 
        $campos=[

   
            'nombre' => 'required|string|max:100',
            'cantidad' => 'required|integer',
            'precio' => 'required|numeric',
            'foto' => 'nullable|max:100000|mimes:jpeg,png,jpg'     

        //     'sku' => 'required|string|max:100',
                    ]; 


        $Mensaje=["required"=> 'El :attribute es requerido'];
       $this->validate($request,$campos,$Mensaje);
            //esta linea es para guardar todo excepto token y method
            //porque laravel los guarda por el mismo           
       $datosProducto=request()->except(['_token', '_method']);
  // aqui voy a update la foto nueva
 
                // if($request->hasFile('Foto')){
                //     $producto= Producto::findOrFail($id);
                //     //esta linea borra la foto vieja especificando la ruta
                //     Storage::delete('public/'.$prodcuto->Foto);
                //     //guardo la nueva foto en mi carpeta publica
                //     $datosProducto['Foto']=$request->file('Foto')->store('uploads','public');
                // }


                 //guardamos la imagen del libro
            if($request->hasFile('foto')){			
                $slug           = substr(strtoupper(sha1(time())),0,6);	
                    
                //guardamos la nueva foto
                $name           = $slug.'.'.$request->file('foto')->getClientOriginalExtension();
                $request->file('foto')->move(public_path().'/img/fotos', $name);
                $datosProducto['foto']   = $name;			           
            }	




        //aca hago el update con todo y foto nueva
        Producto::where('id','=',$id)->update($datosProducto);
        return redirect('productos')->with('Mensaje','Producto modificado con exito');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto= Producto::findOrFail($id);
        //condicionamos si se borro la foto que especificamos
       Storage::delete('public/'.$producto->foto);
        //vamos a borarr foto de de la bd
        Producto::destroy($id);
    

       return redirect('productos')->with('Mensaje','Producto eliminado con exito');
    }
}
