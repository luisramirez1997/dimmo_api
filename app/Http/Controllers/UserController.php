<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function allusers()
    {
    return User::all();
    }

    public function showuser($user)
    {

        $user = User::find($user);

        if(!empty($user) ){
            return $user;
        }else{
            return response([
                "message" => "No encontrado"

            ]);
        }
    
    }

    public function register(Request $request){
            
            $user                = new User();
            $user->name          = $request->name;
            $user->email          = $request->email;
            $user->password   = $request->password;
            $user->save();

            return $user;
    }

      
    public function deleteuser(User $user){

        $user->delete();

return response([
        "message" => "Borrado"

    ]);

        }

}
