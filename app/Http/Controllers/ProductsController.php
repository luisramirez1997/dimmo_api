<?php

namespace App\Http\Controllers;


use Auth;
use App\Producto;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function allproducts()
    {
    return Producto::all();
    }

    public function showproduct($producto)
    {

        $producto = Producto::find($producto);

        if(!empty($producto) ){
            return $producto;
        }else{
            return response([
                "message" => "No encontrado"

            ]);
        }
    
    }

    public function register(Request $request){
            
            $producto                = new Producto();
            $producto->nombre          = $request->nombre;
            $producto->cantidad          = $request->cantidad;
            $producto->descripcion          = $request->descripcion;
            $producto->precio   = $request->precio;
            $producto->sku   = $request->sku;
            $producto->save();

            return $producto;
    }

     
    public function deleteproduct(Producto $product){

        $product->delete();

        return response([
                "message" => "Borrado"
            ]);

    }
}
