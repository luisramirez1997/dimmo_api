<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use Iluminate\Support\Facades\Auth;

use App\User;
use Auth;


class LoginController extends Controller
{

    public function index()
    {
        return User::all();
    }


    public function login(Request $request){


        $credentials = $request->only('email', 'password');

        if( !Auth::attempt($credentials) ){
            return response([
                "message" => "User not valid sorry!!!"

            ],401);
        }


        $accessToken = Auth::user()->createToken('MiToken')->accessToken;

        return response([

        "user" => Auth::user(),
        "access_token" => $accessToken,

            ]);
    }
}
