<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Storage;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['usuarios']=User::paginate(3);

        return view('usuarios.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('usuarios.create',[
            'usuario' => New User
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     //VALIDACION PARA NO DEJAR CAMPOS VACIOS
     $campos=[
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:100',
            'username' => 'required|string|max:100',
            'telefono' => 'required|integer',
            'password' => 'required',
        ]; 

        $Mensaje=["required"=> 'El :attribute es requerido'];
        $this->validate($request,$campos,$Mensaje);
      //  $datosEmpleado=request()->all();

        $datosUsuario=request()->except('_token');

      
       User::insert($datosUsuario);
    //    return response()->json($datosEmpleado);   
    return redirect('usuarios')->with('Mensaje','Usuario agregado con exito');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        return view('usuarios.edit',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                        
            //validacion 
            $campos=[

            'name' => 'required|string|max:100',
            'username' => 'required|string|max:100',
            'telefono' => 'required|integer',
            'email' => 'required|email',
                    ]; 


            $Mensaje=["required"=> 'El :attribute es requerido'];
            $this->validate($request,$campos,$Mensaje);
            //esta linea es para guardar todo excepto token y method
            //porque laravel los guarda por el mismo           
            $datosUsuario=request()->except(['_token', '_method']);
            // aqui voy a update la foto nueva

            

            //aca hago el update con todo y foto nueva
            User::where('id','=',$id)->update($datosUsuario);
            return redirect('usuarios')->with('Mensaje','Producto modificado con exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $usuario= User::findOrFail($id);
        //condicionamos si se borro la foto que especificamos
       
        User::destroy($id);
    

       return redirect('usuarios')->with('Mensaje','Usuario eliminado con exito');
    }
}
