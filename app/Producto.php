<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
   // protected $fillable = ['Nombre','ApellidoPaterno','ApellidoMaterno','Correo','Foto'];

   public function scopeSearch($query,$search){
    return $query->where('nombre','like','%'.$search.'%')
    ->orWhere('sku','like','%'.$search.'%')
    ;

   }


}
