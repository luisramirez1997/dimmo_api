<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Laravel 7.0 with auth 2.0 by using laravel passport, API AND WEBSITE APP




- This laravel app has been built in order to show how easy we can develop and take advantage of this framework while
developing a website app or api with a backend with php and little front end made with boostrap and vue components.


- Login and register via web or api
- Option for "I forget my password" (you get an email to reset it which generates a token with lifetime for 15 minutes)
- Register of data for users and products fields via web or api
- CRUD (CREATE, READ, UPDATE AND DELETE)
- Backend validations to avoid users registering wrong data on our servers.




## Installation STEPS (for using it on your local computer)

- git clone https://gitlab.com/luisramirez1997/dimmo_api.git.
- cp .env.example .env
- config your data base on the .env file(you need to have an existing data base already)
- composer install
- php artisan key:generate
- php artisan migrate:refresh --seed
-  php artisan passport:install
-  npm install
-  npm run dev
- php artisan serve


## USAGE VIA POSTMAN AND WEBSITE

- hrlernesto.ml is the website link to check it out
- Postman collection with all its END POINTS https://www.getpostman.com/collections/19bbe9fc1107683f38d1
- Register via web app or api end point 
- Check the postman collection to check end point by end point.

## ENDPOINTS (BE AWARE YOU NEED TO USE A TOKEN FOR ALL ENDPOINTS AS AN EXEPTION FOR THE NUMBER 1)



-  REGISTER YOURSELF VIA WEB O ENDPOINT (hrlernesto.ml/api/mitoken/register)
- VERB:POST, PARAMETERS:{
    "email": "example@gmail.com",
    "password": "00000000"
}

- To get your token which is valid just for 1 hour (hrlernesto.ml/api/mitoken/login)
- LOGIN 
- VERB:POST, PARAMETERS:{
    "email": "your registered email",
    "password": "your password registerd"
}


- To get all the users data (hrlernesto.ml/api/users/allusers)
- VERB:GET

- To get an specific user data (hrlernesto.ml/api/users/show/id_Number)
- VERB:GET



- To delete an specific product (hrlernesto.ml/api/products/delete/id_number)
- VERB:GET

- To get all the products data (hrlernesto.ml/api/products/allproducts)
- VERB:GET

- To get an specific product data (hrlernesto.ml/api/products/show/id_Number)
- VERB:GET

- To register a product (hrlernesto.ml/api/products/register)
- VERB:POST, PARAMETERS: {
    "sku": "abc",
    "nombre": "laptop",
    "cantidad": "123",
    "precio" : "10"
}

- To delete an specific user (hrlernesto.ml/api/products/delete/id_number)
- VERB:GET

## Developed by

-Luis Ernesto Hernandez Ramirez.
Phone + 503 7601 1036
