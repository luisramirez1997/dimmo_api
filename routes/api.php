<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use Auth;
use Iluminate\Support\Facades\Auth;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


			Route::prefix('/user')->group(function() {

			Route::middleware('auth:api')->get('/', 'MiLoginController@index');
			Route::post('/login', 'MiLoginController@login');
			Route::get('all', 'UserController@all');
			// Route::middleware('auth:api')->get('all', 'UserController@all');
			Route::get('all', 'UserController@all');


			});



		Route::prefix('/mitoken')->group(function() {

				Route::post('/login', 'MiLoginController@login');

		});

		///rutas para prodcutos api

		Route::group(['middleware' => 'auth:api'], function () {

				Route::prefix('/products')->group(function() {

				Route::get('allproducts', 'ProductsController@allproducts');
				Route::get('/show/{product}', 'ProductsController@showproduct');
				Route::get('/delete/{product}', 'ProductsController@deleteproduct');
				Route::post('/register', 'ProductsController@register');

				});	

				//rutas para usuarios

				Route::prefix('/users')->group(function() {

				Route::get('allusers', 'UserController@allusers');
				Route::get('/show/{user}', 'UserController@showuser');
				Route::get('/delete/{user}', 'UserController@deleteuser');
				Route::post('/register', 'UserController@register');

				});	

		});




